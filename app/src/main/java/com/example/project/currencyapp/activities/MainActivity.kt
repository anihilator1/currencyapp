package com.example.project.currencyapp.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import com.android.volley.RequestQueue
import com.example.project.currencyapp.adapters.MainActivityAdapter
import com.example.project.currencyapp.model.MainActivityGridItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.ArrayList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import android.view.MenuItem
import android.widget.AdapterView.AdapterContextMenuInfo
import com.example.project.currencyapp.R

class MainActivity : AppCompatActivity() {

    private var adapter: MainActivityAdapter? = null
    private var currencyList = ArrayList<MainActivityGridItem>()
    private var requestQueue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initGridView()
    }

    private fun initGridView() {
        adapter = MainActivityAdapter(this, getAddCurrencyOnClickListener())
        currencyGrid.adapter = adapter
        loadData()
        registerForContextMenu(currencyGrid);
    }

    private fun loadData() {
        var sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        var jsonString = sharedPreferences.getString("currencyCodeList", null)
        var gson = Gson()
        val type = object : TypeToken<ArrayList<ArrayList<String>>>() {}.type
        if (jsonString == null) return
        var codeList: ArrayList<ArrayList<String>> = gson.fromJson(jsonString, type);

        if (adapter!!.currencyList.isEmpty() && jsonString != "[[]]") {
            for (listOfCodes in codeList) {
                var currencyViewModelList = ArrayList<MainActivityGridItem>();
                for (code in listOfCodes) {
                    currencyViewModelList.add(MainActivityGridItem(code, this.applicationContext))
                }
                adapter!!.currencyList.add(currencyViewModelList)

            }
        }
        adapter!!.notifyDataSetChanged()
    }

    override fun onStop() {
        super.onStop()
        saveData()

    }

    private fun saveData() {
        var sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()

        var dataToSerializeToJson = ArrayList<ArrayList<String>>()
        for (item in adapter!!.currencyList) {
            var list = ArrayList<String>()
            for (currency in item) {
                list.add(currency.name.get()!!);
            }
            dataToSerializeToJson.add(list);
        }
        val gson = Gson()
        var jsonString = gson.toJson(dataToSerializeToJson);

        editor.remove("currencyCodeList");
        editor.putString("currencyCodeList", jsonString)
        editor.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            var list: ArrayList<String> = data!!.getStringArrayListExtra("result")
            if(list.isEmpty()) return
            var resultList = ArrayList<MainActivityGridItem>()
            for (item in list) {
                resultList.add(MainActivityGridItem(item.split(" ")[0], this.applicationContext))

            }
            adapter!!.currencyList.add(resultList)
            adapter!!.notifyDataSetChanged()
        }
    }

    private fun getAddCurrencyOnClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, AddCurrencyActivity::class.java)
            startActivityForResult(intent, 0);
        }
    }

    private fun getChartActivityOnClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, ChartActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        var tmp = v!!.getTag()
        menu!!.add(0, v!!.id, 0, "Delete")
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val info = item!!.menuInfo as AdapterContextMenuInfo
        adapter!!.currencyList.remove(adapter!!.getItem(info.position))
        adapter!!.notifyDataSetChanged()
        return super.onContextItemSelected(item);
    }
}

