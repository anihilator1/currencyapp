package com.example.project.currencyapp.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import com.example.project.currencyapp.R
import com.example.project.currencyapp.databinding.CurrencyListItemBinding
import com.example.project.currencyapp.model.AddCurrencyActivityListItem

class AddCurencyAcitiviyAdapter : BaseAdapter {
    private var context: Context
    public var orginalActivityListAdd: ArrayList<AddCurrencyActivityListItem>
    private var displayActivityListAdd: List<AddCurrencyActivityListItem>

    constructor(context: Context, addCurrencyActivityList: ArrayList<AddCurrencyActivityListItem>) : super() {
        this.context = context
        this.orginalActivityListAdd = addCurrencyActivityList
        this.displayActivityListAdd=addCurrencyActivityList

    }

    fun setNewData(addCurrencyActivityList: List<AddCurrencyActivityListItem>) {
        displayActivityListAdd=addCurrencyActivityList
    }


    override fun getCount(): Int {
        return displayActivityListAdd.size
    }

    override fun getItem(position: Int): Any {
        return displayActivityListAdd[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        //return displayViewList[position];
        var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var currencyView = inflater.inflate(R.layout.currency_list_item, null)
        var binding: CurrencyListItemBinding? =
            DataBindingUtil.bind(currencyView)
        binding!!.currencyModoel = displayActivityListAdd[position]
        binding.executePendingBindings()
        currencyView.findViewById<CheckBox>(R.id.selectedCheckBox).jumpDrawablesToCurrentState()
        currencyView.setOnClickListener({
            displayActivityListAdd[position].selected.set(!(displayActivityListAdd[position].selected.get()!!));
        })
        return currencyView
    }

}