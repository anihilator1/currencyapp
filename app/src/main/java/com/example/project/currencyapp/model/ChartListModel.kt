package com.example.project.currencyapp.model

import android.content.Context
import android.databinding.ObservableField
import android.os.Handler
import android.util.Log
import android.view.View
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class ChartListModel {

    public val name = ObservableField<String>()
    public val code = ObservableField<String>()
    public val date = ObservableField<String>()
    public val mid = ObservableField<String>()
    public val bid = ObservableField<String>()
    public val ask = ObservableField<String>()
    public val number = ObservableField<String>()
    public val context: Context

    constructor(code: String, context: Context) {
        this.code.set(code)
        this.context = context
        getData()
    }

    private fun prepareUri(table: String, code: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        var calendar = Calendar.getInstance()
        val currenDate = format.format(calendar.time)
        calendar.add(Calendar.DAY_OF_YEAR, -10)
        val previousDate = format.format(calendar.time)
        val prefix = "http://api.nbp.pl/api/exchangerates/rates/"
        val postfix = "/?format=json"
        val uri = prefix + table + "/" + code + "/" + previousDate + "/" + currenDate + postfix
        return uri
    }

    private fun getData() {
        val uriA = prepareUri("A", this.code.get()!!)
        val uriB = prepareUri("B", this.code.get()!!)
        val uriC = prepareUri("C", this.code.get()!!)

        val requestQueue = Volley.newRequestQueue(context)
        val errorListner = Response.ErrorListener { error -> Log.v("REQUEST ERROR", "Error") }
        val requestA = JsonObjectRequest(Request.Method.GET, uriA, null, getABResponseListener(), getABErrorListener())
        val requestB = JsonObjectRequest(Request.Method.GET, uriB, null, getABResponseListener(), getABErrorListener())
        val requestC = JsonObjectRequest(Request.Method.GET, uriC, null, getCResponseListener(), getCErrorListener())

        requestQueue.add(requestA)
        requestQueue.add(requestB)
        requestQueue.add(requestC)
    }

    private fun getCErrorListener(): Response.ErrorListener {
        return Response.ErrorListener { error ->
            if (error is NoConnectionError) {
                var handler = Handler()
                val timerRunnable = object : Runnable {

                    override fun run() {
                        getData()
                    }
                }
                handler.postDelayed(timerRunnable, 3000)
            } else {
                if (error.networkResponse == null || error.networkResponse.statusCode != 404) {
                    getData()
                } else {
                    this.bid.set("-")
                    this.ask.set("-")
                }
            }
        }
    }

    private fun getCResponseListener(): Response.Listener<JSONObject> {
        return Response.Listener { response ->
            val table = response.getJSONArray("rates")
            val bidRate = table.getJSONObject(table.length() - 1).getDouble("bid")
            val bidResultRate = (bidRate * 100).roundToInt().toDouble() / 100.0
            this.bid.set(bidResultRate.toString())

            val askRate = table.getJSONObject(table.length() - 1).getDouble("ask")
            val askResultRate = (askRate * 100).roundToInt().toDouble() / 100.0
            this.ask.set(askResultRate.toString())
        }
    }

    private fun getABResponseListener(): Response.Listener<JSONObject> {
        return Response.Listener { response ->
            name.set(response.getString("currency"))


            val table = response.getJSONArray("rates")

            this.date.set(table.getJSONObject(table.length() - 1).getString("effectiveDate"))

            var number = table.getJSONObject(table.length() - 1).getString("no")
            number = number.replace("/C", "").replace("/B", "").replace("/A", "")
            this.number.set(number)

            val rate = table.getJSONObject(table.length() - 1).getDouble("mid")
            val resultRate = (rate * 100).roundToInt().toDouble() / 100.0
            this.mid.set(resultRate.toString())
        }
    }

    private fun getABErrorListener(): Response.ErrorListener {
        return Response.ErrorListener { error ->
            if (error is NoConnectionError) {
                var handler = Handler()
                val timerRunnable = object : Runnable {

                    override fun run() {
                        getData()
                    }
                }
                handler.postDelayed(timerRunnable, 3000)
            } else {
                if (error.networkResponse == null || error.networkResponse.statusCode != 404) {
                    getData()
                }
            }
        }
    }
}