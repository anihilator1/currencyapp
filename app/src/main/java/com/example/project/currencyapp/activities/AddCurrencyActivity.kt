package com.example.project.currencyapp.activities

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.example.project.currencyapp.R
import com.example.project.currencyapp.adapters.AddCurencyAcitiviyAdapter
import com.example.project.currencyapp.helpers.SearchWatcher
import com.example.project.currencyapp.model.AddCurrencyActivityListItem
import kotlinx.android.synthetic.main.activity_add_currency.*
import org.json.JSONArray
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.collections.ArrayList

class AddCurrencyActivity : AppCompatActivity() {
    private var currencyList = ArrayList<AddCurrencyActivityListItem>()
    private var adapter: AddCurencyAcitiviyAdapter? = null
    private var responseList = Collections.synchronizedList<AddCurrencyActivityListItem>(currencyList)
    private var atomicBoolean: AtomicBoolean = AtomicBoolean(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_currency)
        getData()
    }

    private fun getData() {
        val requestQueue = Volley.newRequestQueue(this.applicationContext)

        val tableARequest = "http://api.nbp.pl/api/exchangerates/tables/A/?format=json"
        val tableBRequest = "http://api.nbp.pl/api/exchangerates/tables/B/?format=json"

        val responseListner = getResponseListener()
        val errorListner = getErrorListener()

        val requestA = JsonArrayRequest(Request.Method.GET, tableARequest, null, responseListner, errorListner)
        val requestB = JsonArrayRequest(Request.Method.GET, tableBRequest, null, responseListner, errorListner)

        requestQueue.add(requestA)
        requestQueue.add(requestB)
    }

    private fun getErrorListener(): Response.ErrorListener {
        return Response.ErrorListener { error ->
            if (error is NoConnectionError) {
                var handler = Handler()
                val timerRunnable = object : Runnable {
                    override fun run() {
                        getData()
                    }
                }
                handler.postDelayed(timerRunnable, 3000)
            } else {
                if (error.networkResponse == null || error.networkResponse.statusCode != 404) {
                    getData()
                }
            }
        }
    }

    private fun getResponseListener(): Response.Listener<JSONArray> {
        return Response.Listener<JSONArray> { response ->
            val table = response.getJSONObject(0).getJSONArray("rates")
            for (index in 0..table.length() - 1) {
                var jsonObject = table.getJSONObject(index)
                var code = jsonObject.getString("code")
                var name = jsonObject.getString("currency")
                var model = AddCurrencyActivityListItem(code + " - " + name)
                responseList.add(model)
            }
            if (!atomicBoolean.getAndSet(true)) {
                atomicBoolean.set(true)

            } else {
                adapter = AddCurencyAcitiviyAdapter(this, currencyList)
                currencyListView.adapter = adapter
                searchEditText.addTextChangedListener(
                    SearchWatcher(
                        adapter!!
                    )
                )
                acceptButton.setOnClickListener({ view -> getResult() })
                listProgressBar.visibility = View.GONE
                currencyListView.visibility = View.VISIBLE
            }
        }
    }

    private fun getResult() {
        var resultList = ArrayList<String>()
        for (item in adapter!!.orginalActivityListAdd) {
            if (item.selected.get()!!) {
                resultList.add(item.name.get()!!)
            }
        }
        var output: Intent = Intent()
        output.putExtra("result", resultList)
        setResult(Activity.RESULT_OK, output)
        finish()

    }
}

