package com.example.project.currencyapp.helpers

import android.text.Editable
import android.text.TextWatcher
import com.example.project.currencyapp.adapters.AddCurencyAcitiviyAdapter
import com.example.project.currencyapp.model.AddCurrencyActivityListItem
import java.util.function.Predicate
import java.util.stream.Collectors

class SearchWatcher : TextWatcher {
    private var adapter: AddCurencyAcitiviyAdapter
    private var addCurrencyActivityList: List<AddCurrencyActivityListItem>

    constructor(adapter: AddCurencyAcitiviyAdapter) {
        this.adapter = adapter
        addCurrencyActivityList = adapter.orginalActivityListAdd
    }

    override fun afterTextChanged(s: Editable?) {
        return
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        return
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        var resultActivityListAdd: List<AddCurrencyActivityListItem>

        if (s.isNullOrBlank()) {
            resultActivityListAdd = addCurrencyActivityList
        } else {
            resultActivityListAdd = addCurrencyActivityList.stream()
                .filter(Predicate { t: AddCurrencyActivityListItem -> t.name.get()!!.contains(s!!, true) })
                .collect(Collectors.toList())
        }
        adapter.setNewData(resultActivityListAdd)
        adapter.notifyDataSetChanged()
    }

}