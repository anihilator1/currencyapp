package com.example.project.currencyapp.model

import android.databinding.ObservableField

class AddCurrencyActivityListItem {
    public var name=ObservableField<String>()
    public var selected=ObservableField<Boolean>(false)
    constructor(name:String){
        this.name.set(name)
    }
}