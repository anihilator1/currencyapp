package com.example.project.currencyapp.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.project.currencyapp.R
import com.example.project.currencyapp.databinding.ChartListEntryBinding
import com.example.project.currencyapp.model.ChartListModel

class ChartActivityListAdapter : BaseAdapter {

    private var dataList = ArrayList<ChartListModel>()
    protected var viewList = ArrayList<View>()
    var context: Context

    constructor(context: Context) : super() {
        this.context = context
    }

    fun addData(model: ChartListModel) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.chart_list_entry, null)
        dataList.add(model)
        viewList.add(view)
        val binding: ChartListEntryBinding? =
            DataBindingUtil.bind(view)
        binding!!.model = model
        this.notifyDataSetChanged()
    }


    override fun getCount(): Int {
        return viewList.size
    }

    override fun getItem(position: Int): Any {
        return viewList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return viewList[position];
    }
}