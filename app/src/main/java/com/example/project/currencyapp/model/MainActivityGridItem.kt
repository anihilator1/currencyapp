package com.example.project.currencyapp.model

import android.content.Context
import android.databinding.ObservableByte
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Handler
import android.util.Log
import android.view.View
import com.android.volley.NoConnectionError
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

public class MainActivityGridItem {
    public var name: ObservableField<String> = ObservableField()
    public var rate: ObservableField<String> = ObservableField()
    public var change: ObservableField<String> = ObservableField()
    public var time: ObservableField<String> = ObservableField()
    public var contentVisibility = ObservableInt(View.GONE)
    public var progerssBarVisibility = ObservableInt(View.VISIBLE)
    private var context: Context? = null

    constructor(name: String, context: Context) {
        this.name.set(name)
        this.rate.set("1.4")
        this.change.set("1.3%")
        this.time.set("25.12.2018")
        this.context = context
        getData()
    }

    private fun prepareUri(table: String, code: String): String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        var calendar = Calendar.getInstance()
        val currenDate = format.format(calendar.time)
        calendar.add(Calendar.DAY_OF_YEAR, -20)
        val previousDate = format.format(calendar.time)
        val prefix = "http://api.nbp.pl/api/exchangerates/rates/"
        val postfix = "/?format=json"
        val uri = prefix + table + "/" + code + "/" + previousDate + "/" + currenDate + postfix
        return uri
    }

    private fun getData() {
        val uriA = prepareUri("A", this.name.get()!!)
        val uriB = prepareUri("B", this.name.get()!!)

        val requestQueue = Volley.newRequestQueue(context)
        val errorListner = getErrorListener()
        val requestA = JsonObjectRequest(Request.Method.GET, uriA, null, getResponseListener(), errorListner)
        val requestB = JsonObjectRequest(Request.Method.GET, uriB, null, getResponseListener(), errorListner)
        requestQueue.cache.clear()

        requestQueue.add(requestA)
        requestQueue.add(requestB)
    }

    private fun getResponseListener(): Response.Listener<JSONObject> {
        return Response.Listener { response ->
            val table = response.getJSONArray("rates")
            val rate1 = table.getJSONObject(table.length() - 1).getDouble("mid")
            val rate2 = table.getJSONObject(table.length() - 2).getDouble("mid")
            val resultRate = (rate1 * 100).roundToInt().toDouble() / 100.0
            this.rate.set(resultRate.toString())
            val change = (rate1 - rate2) / rate2 * 100
            val resultChange = (change * 100).roundToInt().toDouble() / 100
            this.change.set(resultChange.toString() + "%")
            contentVisibility.set(View.VISIBLE)
            progerssBarVisibility.set(View.GONE)
        }
    }

    private fun getErrorListener(): Response.ErrorListener {
        return Response.ErrorListener { error ->
            if (error is NoConnectionError) {
                var handler = Handler()
                val timerRunnable = object : Runnable {

                    override fun run() {
                        getData()
                    }
                }
                handler.postDelayed(timerRunnable, 3000)
            } else {
                if (error.networkResponse == null || error.networkResponse.statusCode != 404) {
                    getData()
                }

            }

        }
    }
}