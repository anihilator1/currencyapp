package com.example.project.currencyapp.activities

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.NoConnectionError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.project.currencyapp.R
import com.example.project.currencyapp.adapters.ChartActivityListAdapter
import com.example.project.currencyapp.model.ChartListModel
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.activity_chart.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ChartActivity : AppCompatActivity() {

    private var chartData: ArrayList<ILineDataSet>? = null
    private var startDateCalendar = Calendar.getInstance()
    private var endDateCalendar: Calendar = Calendar.getInstance()
    private var codeList: ArrayList<String>? = null
    private var listAdapter: ChartActivityListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)

        startDateCalendar.add(Calendar.DAY_OF_YEAR, -20)

        codeList = intent.getStringArrayListExtra("codeList")
        getData(codeList!!)

        setAxisX()

        chartData = ArrayList()
        chart.data = LineData(chartData)

        chart.invalidate()

        initDate()

        initList()
        generateListData()
    }

    private fun generateListData() {
        for (code in codeList!!) {
            listAdapter!!.addData(ChartListModel(code, this))
        }
    }

    private fun getData(codeList: ArrayList<String>) {
        for (code in codeList) {
            generateRequest(code)
        }
    }

    private fun getDatePickerListener(textView: TextView, calendar: Calendar,from:Calendar,to:Calendar,context:Context): DatePickerDialog.OnDateSetListener {
        var listner = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            var tmp=calendar.time
            var now=Calendar.getInstance()
            calendar.set(year, month, dayOfMonth)
            if(from.time.after(to.time)||now.before(calendar))
            {
                Toast.makeText(context, "Invalid date range",
                    Toast.LENGTH_LONG).show();
                calendar.time=tmp;
                return@OnDateSetListener
            }
            textView.text = getDate(calendar)
            chartData!!.clear()
            chart.visibility = View.GONE
            chartProgressBar.visibility = View.VISIBLE
            getData(codeList!!)
        }
        return listner
    }


    private fun initDate() {
        var calendar = Calendar.getInstance()
        endDateTextView.text = getDate(calendar)
        calendar.add(Calendar.DAY_OF_YEAR, -20)
        startDateTextView.text = getDate(calendar)
        startDateTextView.setOnClickListener(View.OnClickListener {
            val dialog = DatePickerDialog(
                this,
                getDatePickerListener(startDateTextView, startDateCalendar,startDateCalendar,endDateCalendar,this),
                startDateCalendar.get(Calendar.YEAR),
                startDateCalendar.get(Calendar.MONTH),
                startDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        })
        endDateTextView.setOnClickListener {
            val dialog = DatePickerDialog(
                this,
                getDatePickerListener(endDateTextView, endDateCalendar,startDateCalendar,endDateCalendar,this),
                endDateCalendar.get(Calendar.YEAR),
                endDateCalendar.get(Calendar.MONTH),
                endDateCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun setAxisX() {
        val format = SimpleDateFormat("dd-MM-yyyy")
        val valueFomater = IAxisValueFormatter { value, axis ->
            val tmp = value.toLong()
            var m = TimeUnit.HOURS.toMillis(tmp)
            return@IAxisValueFormatter format.format(m)
        }
        chart.xAxis.valueFormatter = valueFomater
        chart.invalidate()
    }

    private fun generateRequest(code: String) {
        val requestQueue = Volley.newRequestQueue(this.applicationContext)

        val uriA = prepareUri("A", code, startDateCalendar, endDateCalendar)
        val uriB = prepareUri("B", code, startDateCalendar, endDateCalendar)

        val requestA =
            JsonObjectRequest(Request.Method.GET, uriA, null, getResponseListener(code), getErrorListener(code))
        val requestB =
            JsonObjectRequest(Request.Method.GET, uriB, null, getResponseListener(code), getErrorListener(code))

        requestQueue.add(requestA)
        requestQueue.add(requestB)
    }

    private fun getErrorListener(code: String): Response.ErrorListener {
        return Response.ErrorListener { error ->
            if (error is NoConnectionError) {
                var handler = Handler()
                val timerRunnable = object : Runnable {

                    override fun run() {
                        generateRequest(code)
                    }
                }
                handler.postDelayed(timerRunnable, 4000)
            } else {
                if (error.networkResponse == null || error.networkResponse.statusCode != 404) {
                    generateRequest(code)
                }
            }
        }
    }

    private fun getResponseListener(code: String): Response.Listener<JSONObject> {
        return Response.Listener { response ->
            val table = response.getJSONArray("rates")
            val entries = ArrayList<Entry>()
            for (index in 0..table.length() - 1) {
                var rate = table.getJSONObject(index).getDouble("mid")
                var date = table.getJSONObject(index).getString("effectiveDate")
                var calendar = Calendar.getInstance()
                var dateTable = date.split("-")
                calendar.set(dateTable[0].toInt(), dateTable[1].toInt() - 1, dateTable[2].toInt())
                var milis = calendar.time.time
                var hours = TimeUnit.MILLISECONDS.toHours(milis)
                entries.add(Entry(hours.toFloat(), rate.toFloat()))
            }
            val dataSet = LineDataSet(entries, code)
            val lineData = LineData(dataSet)
            chart.data = lineData
            var random = Random()
            dataSet.setColor(Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255)))
            dataSet.lineWidth = 2f
            if(chartData!!.find{ set->set.label==code}==null)
                chartData!!.add(dataSet)
            chart.data = LineData(chartData)
            chart.invalidate()
            chart.visibility = View.VISIBLE
            chartProgressBar.visibility = View.GONE
        }
    }

    private fun prepareUri(table: String, code: String, calendarFrom: Calendar, calendarTo: Calendar): String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        val currenDate = format.format(calendarTo.time)
        val previousDate = format.format(calendarFrom.time)
        val prefix = "http://api.nbp.pl/api/exchangerates/rates/"
        val postfix = "/?format=json"
        val uri = prefix + table + "/" + code + "/" + previousDate + "/" + currenDate + postfix
        return uri
    }

    private fun getDate(date: Calendar): String {
        val format = SimpleDateFormat("dd-MM-yyy")
        return format.format(date.time)
    }

    private fun initList() {
        listAdapter = ChartActivityListAdapter(this)
        listView.adapter = listAdapter
    }


}
