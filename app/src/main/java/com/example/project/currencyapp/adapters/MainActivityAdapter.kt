package com.example.project.currencyapp.adapters

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.project.currencyapp.activities.ChartActivity
import com.example.project.currencyapp.R
import com.example.project.currencyapp.databinding.CurrencyEntryBinding
import com.example.project.currencyapp.model.MainActivityGridItem

class MainActivityAdapter : BaseAdapter {
    var currencyList = ArrayList<ArrayList<MainActivityGridItem>>()
    var context: Context? = null
    var addClickListener: View.OnClickListener

    constructor(context: Context, onClickListener: View.OnClickListener) : super() {
        this.context = context
        this.addClickListener = onClickListener
    }

    override fun getCount(): Int {
        return currencyList.size + 1
    }

    override fun getItem(position: Int): Any {
        if(position==currencyList.size) return currencyList[0];
        return currencyList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (currencyList.size > position) {
            val currency = currencyList[position]
            var currencyView = inflater.inflate(R.layout.currency_entry, null)


            currencyView.isLongClickable = true;

            var binding: CurrencyEntryBinding? = DataBindingUtil.bind(currencyView)
            binding!!.model = currency[0]

            initTimeChanging(currency, binding)
            initOnClickListener(currency, currencyView)

            return currencyView;
        } else {

            var addCurrencyView = inflater.inflate(R.layout.add_currency_entry, null);
            addCurrencyView.setOnClickListener(addClickListener)
            return addCurrencyView
        }
    }

    private fun initOnClickListener(currency: ArrayList<MainActivityGridItem>, currencyView: View) {
        var codeList = ArrayList<String>()
        for (item in currency) {
            codeList.add(item.name.get()!!)
        }
        currencyView.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, ChartActivity::class.java)
            intent.putExtra("codeList", codeList)
            context!!.startActivity(intent)
        })

    }

    private fun initTimeChanging(currency: ArrayList<MainActivityGridItem>, binding: CurrencyEntryBinding) {
        var position: Int = 0
        var handler = Handler()
        val timerRunnable = object : Runnable {

            override fun run() {
                position = (position + 1) % currency.size
                var model = currency[position]
                binding.model = model
                //binding.notifyChange()
                handler.postDelayed(this, 3000)
            }
        }
        handler.postDelayed(timerRunnable, 3000)
    }

}